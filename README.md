# 3PO Light Kit

## Getting started

Hi and thanks for your interest in the 3PO light kit. If you wish to edit the code and customise your kit you'll be able to do so from here. Changes to the config will allow you change the colour or brightness for example. Simply follow the instructions below and you are away to edit at yourn leisure :-)

-download the sketch
-download and run arduino IDE https://www.arduino.cc/en/software
-In Tools, select Manage Libraries, download Adafruit NeoPixel and Bounce2. Using filters may help. Lines 1 and 2 of the code also include this information. 
-Once downloaded edit the configuration and simply conenct the pro micro to your laptop and hit upload.
-The default pro micro config will be erased and overwritten but the original config will always be stored here if you need to refer back to it.

## Support
Please note support on specific changes to the config is limited but should you have any questions please contact me via my website: https://chrismakes.uk/contact-chris/

## Roadmap
Future releases will include a wireless or bluetooth option, allowing users to change the colour of the leds on the fly for example.

