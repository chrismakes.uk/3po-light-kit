#include <Adafruit_NeoPixel.h>
#include <Bounce2.h>


#define LED_PIN    9
#define LED_COUNT 6
#define BUTTON_PIN 8

#define BRIGHTNESS 65

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

uint32_t white_colour = strip.Color(255,   193,   70);
uint32_t std_colour = strip.Color(255,   193,   20);
uint32_t red_colour = strip.Color(255,   0,   0);

Bounce2::Button button = Bounce2::Button();

int ledMode=0;

void setup() {

  Serial.begin(115200);
  // put your setup code here, to run once:
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(BRIGHTNESS); // Set BRIGHTNESS to about 1/5 (max = 255)
  for(int i=0; i<strip.numPixels(); i++)
  {
    strip.setPixelColor(i, white_colour);
    strip.show();
  }
  button.attach( BUTTON_PIN, INPUT_PULLUP );
  button.interval(20); 
  button.setPressedState(LOW);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  button.update();
  if ( button.pressed() ) {
    ledMode++;
    if ( ledMode == 3) {
      ledMode = 0;
    }
    switch (ledMode) {
      case 0:
        setColour(white_colour);
        break;
      case 1:
        setColour(std_colour);
        break;
      case 2:
        setColour(red_colour);
        break;
    }
  }
  Serial.println(ledMode);


}

void setColour(uint32_t colour) {
  for(int i=0; i<strip.numPixels(); i++)
  {
    strip.setPixelColor(i, colour);
    strip.show();
  }
}
